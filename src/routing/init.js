import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { createSwitchNavigator, createAppContainer, StackActions } from 'react-navigation'

import AppWrap from '../screens/appWrap'
import Auth from '../screens/auth'
import HomeScreen from '../screens/HomeScreen'
import Dialogs from '../screens/main/dialogs/index.js'
import Settings from '../screens/main/settings/index'
import Chat from '../screens/main/chat/index'
import Contacts from '../screens/main/contacts/index'
import CreateDialog from '../screens/main/contacts/createDialog'
import GroupDetails from '../screens/main/chat/groupDetails'
import ContactDetails from '../screens/main/chat/contactDetails'

export default createAppContainer (createSwitchNavigator(
    {
        AppWrap,
        Auth: createStackNavigator({
            Auth: {
              screen: Auth,
              navigationOptions: {
                headerTitle: 'Auth',
              }
            }
          }),
          HomeScreen,
          Main: createStackNavigator({
            Dialogs: {
              screen: Dialogs
            },
            Settings: {
              screen: Settings,
              navigationOptions: {
                headerTitle: 'Settings',
              }
            },
            Chat: {
              screen: Chat,
            },
            Contacts: {
              screen: Contacts,
              navigationOptions: {
                headerTitle: 'Contacts'
              }
            },
            CreateDialog: {
              screen: CreateDialog,
              navigationOptions: {
                headerTitle: 'New Group'
              }
            },
            GroupDetails: {
              screen: GroupDetails,
              navigationOptions: {
                headerTitle: 'Group details'
              }
            },
            ContactDetails: {
              screen: ContactDetails,
              navigationOptions: {
                headerTitle: 'Contact details'
              }
            }
          })
    },
    {
      initialRouteName: 'AppWrap',
    }
))