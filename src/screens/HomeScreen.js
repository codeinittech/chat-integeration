import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  chat = () => {
    ChatService.setUpListeners()
					this.setState({ isLoader: false })
					navigation.navigate('Dialogs')
  }

  render() {
    return (
      <View>
        <Text> HomeScreen </Text>
        <Button title= 'Go to chat' onPress={() => this.props.navigation.navigate('Dialogs')}/>
      </View>
    );
  }
}
